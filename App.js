import React, { useState } from 'react';
import {
  StyleSheet,
  Image,
  View,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Text,
} from 'react-native';

const images = [
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-8.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-10.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-9.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-5.jpg'

]

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;


const App = () => {

  const [ImgAcvive, setImgActive] = useState(0);

  onchange = (nativeEvent) => {
    if (nativeEvent) {
      const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
      if (slide != ImgAcvive) {
        setImgActive(slide);
      }
    }

  }

  return (

    <SafeAreaView style={styles.container} >
      <View style={styles.wrap}>

        <ScrollView
          onScroll={({ nativeEvent }) => onchange(nativeEvent)}
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          fadingEdgeLength={0}
          // indicatorStyle={'white'}
          // persistentScrollbar={true}
          style={styles.wrap}
        >
          {
            images.map((e, index) =>
              <Image
                key={e}
                resizeMode='stretch'
                style={styles.wrap}
                source={{ uri: e }} />
            )}

        </ScrollView>

        <View style={styles.wrapHeart}>
          {
            images.map((e, index) =>
              <Text
                key={e}
                style={ImgAcvive == index ? styles.HeartActive : styles.heart}
              >●</Text>
            )
          }

        </View>

      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1
  },

  wrap: {
    width: WIDTH,
    height: HEIGHT * 0.35
  },

  wrapHeart: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'center'
  },

  HeartActive: {
    margin: 2,
    color: '#31C6D4',

  },

  heart: {
    margin: 2,
    color: 'white',
  },



});

export default App;
